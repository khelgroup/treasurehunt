// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
   // apiUrl: 'http://13.235.2.142:3011/api/v1',
  //  apiUrl: 'http://192.168.7.71:3011/api/v1',
  apiUrl: 'http://localhost:3011/api/v1',
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYW1pdHRrZyIsImEiOiJjanV0b29wMDMwOWk5M3ludms5MGUwZml6In0.Nn7cnruE98kLxiAt18UisA'
  }
};
