export const environment = {
  production: true,
  // apiUrl: 'http://localhost:3000/api/v1'
  apiUrl: 'http://localhost:3011/api/v1',
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYW1pdHRrZyIsImEiOiJjanV0b29wMDMwOWk5M3ludms5MGUwZml6In0.Nn7cnruE98kLxiAt18UisA'
  }
};
