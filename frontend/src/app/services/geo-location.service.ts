import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GeoLocationService {

    coordinates: any;
    api = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getPosition(): Observable<Position> {
        return Observable.create(
            (observer) => {
                navigator.geolocation.watchPosition((pos: Position) => {
                    observer.next(pos);
                }),
                    () => {
                        console.log('Position is not available');
                    },
                    {
                        enableHighAccuracy: true
                    };
            });
    }

    getUserSavelocations(pageNumber, pageSize) {
        return this.http.get<any>(this.api + '/maps?pg=' + pageNumber + '&pgsize=' + pageSize);
    }

    getCompetitionList() {
        return this.http.get<any>(this.api + '/competitions');
    }

    getLocations(amountDataId) {
        console.log('amountDataId', amountDataId);
        return this.http.get<any>(this.api + '/distributedLocations?amountDataId=' + amountDataId);
    }

    getRecord(id) {
        return this.http.get<any>(this.api + '/maps/' + id);
    }

    addUserLocation(data) {
        return this.http.post<any>(this.api + '/maps/create', data);
    }

    updateMarker(data) {
        return this.http.post<any>(this.api + '/distributedLocations/multipleupdate', data);
    }

    editUserLocation(data) {
        return this.http.put<any>(this.api + '/maps/update', data);
    }

    deleteUserLocation(data) {
        return this.http.post<any>(this.api + '/maps/delete', data);
    }
}
