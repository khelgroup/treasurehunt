import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {NotificationService} from '../services/notification.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    user: any = {};

    constructor(private userService: UserService,
                private notificationService: NotificationService,
                private router: Router) {
    }

    ngOnInit() {
        let accessToken = localStorage.getItem('token');
        if (!accessToken) {
            this.router.navigate(['/login']);
        } else {
            this.router.navigate(['/dashboard']);
        }
    }

    userLogin() {
        this.userService.login(this.user)
            .subscribe((response) => {
                if (!response.error) {
                    this.user = {};
                    localStorage.setItem('token', response.data.authToken);
                    localStorage.setItem('userId', response.data.userDetails.userId);
                    this.router.navigate(['/dashboard']);
                    this.notificationService.showNotification(response.message, 'success');
                } else {
                    this.notificationService.showNotification(response.message, 'danger');
                }
            });
    }
}
