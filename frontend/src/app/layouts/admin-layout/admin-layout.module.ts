import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LbdModule} from '../../lbd/lbd.module';
import {NguiMapModule} from '@ngui/map';

import {AdminLayoutRoutes} from './admin-layout.routing';
import {HomeComponent} from '../../home/home.component';
import {UserComponent} from '../../user/user.component';
import {TablesComponent} from '../../tables/tables.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import {AgmDirectionModule} from 'agm-direction';
import {AgmCoreModule} from '@agm/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
// import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import {AgmJsMarkerClustererModule} from '@agm/js-marker-clusterer';
import {TreasureComponent} from '../../treasure/treasure.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        LbdModule,
        GooglePlaceModule,
        AgmDirectionModule,
        NgxDatatableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBACbcQmVK1bXTnIGZVCPewFGWLP9j1fWU',
            libraries: ['places']
        }),
        AgmJsMarkerClustererModule
        // NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
    ],
    declarations: [
        HomeComponent,
        UserComponent,
        TablesComponent,
        TypographyComponent,
        IconsComponent,
        NotificationsComponent,
        UpgradeComponent,
        // TreasureComponent
    ]
})

export class AdminLayoutModule {
}
