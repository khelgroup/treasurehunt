import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {TreasureComponent} from './treasure/treasure.component';

const routes: Routes = [
    {path: '', redirectTo: 'treasure', pathMatch: 'full'},
    {path: 'treasure', component: TreasureComponent},
    {path: '**', redirectTo: 'treasure'}
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [],
})
export class AppRoutingModule {
}
