import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TreasureComponent} from './treasure.component';
import {NavbarModule} from '../shared/navbar/navbar.module';
import {FooterModule} from '../shared/footer/footer.module';
import {NgCircleProgressModule} from 'ng-circle-progress';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NavbarModule,
        FooterModule,
        NgCircleProgressModule.forRoot({
            radius: 100,
            outerStrokeWidth: 16,
            innerStrokeWidth: 8,
            outerStrokeColor: '#78C000',
            innerStrokeColor: '#C7E596',
            animationDuration: 300,
        })
    ],
    declarations: [
        TreasureComponent
    ]
})
export class TreasureModule {
}
