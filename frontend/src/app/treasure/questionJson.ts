export const questions = [
    {
        'question': 'What is the color of Banana?',
        'answer': 'Yellow',
        'option': [
            'Yellow',
            'Red',
            'Blue',
            'Green'
        ]
    },
    {
        'question': 'What is the color of Apple?',
        'answer': 'Red',
        'option': [
            'Yellow',
            'Red',
            'Blue',
            'Green'
        ]
    },
    {
        'question': 'What is the color of Orange?',
        'answer': 'Orange',
        'option': [
            'Yellow',
            'Red',
            'Orange',
            'Green'
        ]
    },
    {
        'question': 'January 26 is celebrated as the',
        'answer': 'Republic Day',
        'option': [
            'Republic Day',
            'Ugadhi',
            'Teachers\' Day',
            'Army Day'
        ]
    },
    {
        'question': 'August 15 is celebrated as the',
        'answer': 'Independent Day',
        'option': [
            'Republic Day',
            'Independent Day',
            'Teachers Day',
            'Army Day'
        ]
    },
    {
        'question': 'Teacher\'s Day is observed on which of the date?',
        'answer': 'September 5',
        'option': [
            'September 5',
            'January 30',
            'November 14',
            'October 2'
        ]
    }
];
