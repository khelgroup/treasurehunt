import {Component, ElementRef, OnInit} from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import {environment} from '../../environments/environment';
import {GeoLocationService} from '../services/geo-location.service';
import {QuizService} from '../services/quiz.service';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '../services/notification.service';

@Component({
    selector: 'app-treasure',
    templateUrl: './treasure.component.html',
    styleUrls: ['./treasure.component.scss']
})
export class TreasureComponent implements OnInit {

    map;
    geojson = {
        type: 'FeatureCollection',
        features: []
    };
    varpooja;
    lat;
    lon: any = '';
    display = 'none';
    display1 = 'none';
    alert1 = 'none';
    alert2 = 'none';
    alert3 = 'none';
    location = 'none';
    answer;
    modelData: any = [];
    radioButtonValues: Array<any> = [];
    correctAnswer = null;
    checkedOption;
    questionDone: any = [];
    showWinMessage = false;
    zoom = 15;
    geoAry = [];
    showMap = true;

    constructor(private geoLocationService: GeoLocationService,
                private quizService: QuizService,
                private notificationService: NotificationService,
                private http: HttpClient) {
        mapboxgl.accessToken = environment.mapbox.accessToken
    }

    ngOnInit() {
        window.scrollTo(0, 418);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position => {
                this.location = 'none';
                this.showMap = false;
                this.lat = position.coords.latitude.toFixed(4);
                this.lon = position.coords.longitude.toFixed(4);
                this.main();
            }), (error) => {
                this.showMap = true;
                this.location = 'block';
            });
        } else {
            alert('Geolocation is not supported by this browser.');
        }
        /* navigator.geolocation.getCurrentPosition(position => {
             this.location = 'none';
             this.lat = position.coords.latitude.toFixed(4);
             this.lon = position.coords.longitude.toFixed(4);
             this.main();
         });*/
    }

    showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                this.location = 'block';
                // alert('User denied the request for Geolocation.')
                break;
            case error.POSITION_UNAVAILABLE:
                this.location = 'block';
                // alert('Location information is unavailable.')
                break;
            case error.TIMEOUT:
                this.location = 'block';
                // alert('The request to get user location timed out.')
                break;
            case error.UNKNOWN_ERROR:
                this.location = 'block';
                // alert('An unknown error occurred.')
                break;
        }
    }

    showPosition(position) {
        let a = position.coords.latitude.toFixed(4);
        console.log('Inside position', position)
        console.log('lat', position.coords.latitude.toFixed(4))
        console.log('aaa', a)
        this.varpooja = a;
        // this.lat = position.coords.latitude.toFixed(4);
        // this.lon = position.coords.longitude.toFixed(4);
        // this.main();
    }

    main() {
        let marker1;
        const that = this;
        this.map = new mapboxgl.Map({
            container: 'map',
            center: [this.lon, this.lat],
            zoom: that.zoom,
            style: 'mapbox://styles/amittkg/cjxzmkz4i0ags1cryipim8nv0',
            pitch: 60, // pitch in degrees
            bearing: -160, // bearing in degrees
        });

        marker1 = new mapboxgl.Marker()
            .setLngLat([this.lon, this.lat])
            .addTo(this.map);

        this.map.scrollZoom.disable();


        const number = this.newrandomExcluded([0, 1]);


        const rad = [0, 500];
        for (let i = 0; i < number; i++) {
            const json = {
                type: 'Feature',
                properties: {
                    'message': 'Foo',
                    'iconSize': [60, 60]
                },
                geometry: {
                    type: 'Point',
                    coordinates: []
                }
            }
            let radis = rad[1];
            if (i === 0) {
                radis = rad[0];
            }
            json.geometry.coordinates = this.getLocation(Number(this.lon), Number(this.lat), radis);
            that.geoAry.push({x: json.geometry.coordinates[0], y: json.geometry.coordinates[1], z: 'div' + i});
            this.geojson.features.push(json);
        }

        const geoLocate = (new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true,
                watchPosition: true
            },
            trackUserLocation: true,
            showUserLocation: true,
            /*fitBoundsOptions: {
                maxZoom: that.zoom
            }*/
        }));
        this.map.addControl(geoLocate);

        setTimeout(function () {
            geoLocate._geolocateButton.click();
        }, 800);

        setTimeout(() => {
            this.geojson.features.forEach(function (marker, i) {
                const el = document.createElement('div');
                el.id = that.geoAry[i].z.toString();

                // el.onclick = function () {
                //     that.notificationService.showNotification('Go Here To Collect Your Treasure', 'danger');
                // }

                el.style.backgroundImage = 'url(/assets/img/modal-img/01_box_shine_treasure_Glow_60x60.gif)';
                el.style.width = marker.properties.iconSize[0] + 'px';
                el.style.height = marker.properties.iconSize[1] + 'px';

                new mapboxgl.Marker(el)
                    .setLngLat(marker.geometry.coordinates)
                    .addTo(that.map);
            });
        }, 2000)

        setTimeout(() => {
            that.alert1 = 'block';
        }, 4000)

        geoLocate.on('geolocate', function (e) {
            const liveLat = e.coords.latitude.toFixed(4);
            const liveLon = e.coords.longitude.toFixed(4);
            that.map.bearing = that.computeAngle(that.lon, that.lat);
            marker1.remove();

            setTimeout(() => {
                that.geoAry.forEach((geo, i) => {
                    const distance = that.geodistance(geo.y, geo.x, liveLat, liveLon, 'M');
                    if (Number(distance.toFixed(4)) < 0.0050) {
                        if (document.getElementById(geo.z) !== null) {
                            if (geo.z !== 'div0') {
                                that.display = 'block';
                                document.getElementById(geo.z).remove();
                                that.geoAry.slice(i, 1);
                            }
                            const randomNumber = that.randomExcluded(that.questionDone);
                            const questions = that.quizService.getQuestion();

                            that.questionDone.push(randomNumber);
                            that.modelData = questions[randomNumber];
                            that.answer = that.modelData.answer;
                        }

                    }
                });
            }, 9000);
        });
    }

    popUp1() {
        this.alert1 = 'none';
        setTimeout(() => {
            this.alert2 = 'block';
        }, 1000)
    }

    popUp2() {
        this.alert2 = 'none';
        setTimeout(() => {
            this.display = 'block';
            document.getElementById('div0').remove();
            this.geoAry.slice(0, 1);
        }, 3000)
    }

    popUp3() {
        this.alert3 = 'none';
    }

    computeAngle(pointA, pointB) {
        const dLon = (pointB.x - pointA.x) * Math.PI / 180;
        const lat1 = pointA.y * Math.PI / 180;
        const lat2 = pointB.y * Math.PI / 180;
        const y = Math.sin(dLon) * Math.cos(lat2);
        const x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        let bearing = Math.atan2(y, x) * 180 / Math.PI;
        bearing = Number(((bearing + 360) % 360).toFixed(1)); //Converting -ve to +ve (0-360)
        return bearing
    }

    getLocation(x0, y0, radius) {
        let random = Math.random();
        let random1 = Math.random();

        random = Number(random.toFixed(4));
        random1 = Number(random1.toFixed(4));

        // Convert radius from meters to degrees
        const radiusInDegrees = radius / 111000;

        const u = random * 10;
        const v = random1 * 10;
        const w = radiusInDegrees * Math.sqrt(u);
        const t = 2 * Math.PI * v;
        const x = w * Math.cos(t);
        const y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        const new_x = x / Math.cos(y0 * Math.PI / 180);

        const foundLongitude = Number((new_x + x0).toFixed(4));
        const foundLatitude = Number((y + y0).toFixed(4));
        return [foundLongitude, foundLatitude]
    }

    geodistance(lat1, lon1, lat2, lon2, unit) {
        if ((lat1 === lat2) && (lon1 === lon2)) {
            return 0;
        } else {
            const radlat1 = Math.PI * lat1 / 180;
            const radlat2 = Math.PI * lat2 / 180;
            const theta = lon1 - lon2;
            const radtheta = Math.PI * theta / 180;
            let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit === 'K') {
                dist = dist * 1.609344
            }
            if (unit === 'N') {
                dist = dist * 0.8684
            }
            return Number(dist.toFixed(4));
        }
    }

    onCloseHandled() {
        this.display = 'none';
        this.modelData = {};
        this.answer = '';
        this.correctAnswer = null;
    }

    randomExcluded(excluded) {
        const n = Math.floor(Math.random() * 6);
        return (excluded.indexOf(n) !== -1) ? this.randomExcluded(excluded) : n;
    }

    newrandomExcluded(excluded) {
        const n = Math.floor(Math.random() * 3);
        return (excluded.indexOf(n) !== -1) ? this.randomExcluded(excluded) : n;
    }

    checkValue(value) {
        this.checkedOption = value;

        if (this.checkedOption === this.answer) {
            this.correctAnswer = 'true';
            setTimeout(() => {
                this.display = 'none';
                this.display1 = 'block';
                this.modelData = {};
                this.answer = '';
                this.correctAnswer = null;
                setTimeout(() => {
                    this.showWinMessage = true;
                }, 1000);
                this.onModalClose();
            }, 2000)

        } else {
            this.correctAnswer = 'false';
        }
    }

    onModalClose() {
        if (this.display1 === 'block') {
            setTimeout(() => {
                this.display1 = 'none';
                this.alert3 = 'block';
            }, 5000);
        }
    }
}
